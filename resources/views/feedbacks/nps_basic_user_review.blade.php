<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Acadgild</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Raleway', sans-serif;
                font-weight: 100;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 36px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 12px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
            .rating {
                font-size: 48px;
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            

            <div class="content">
                @if(isset($isreviewed) && $isreviewed)
                <div class="title m-b-md">
                    You had already reviewed.
                </div>
                @elseif(isset($showComment) && $showComment)
                <div class="title m-b-md">
                    You gave us a rating of <span class="rating">{{ $notification->data['rating'] }}</span>
                </div>

                    <form id="survey-form" class="form" action="/notification-comment" method="get" role="form">
            
                <input name="notification_id" type="hidden" value="{{ $notification->id }}">
                <input name="rating" type="hidden" value="{{ $notification->data['rating'] }}">
             
                <hr class="split">
                
                
                <div class="form-group">
                    <label for="id_comment" class="title">Tell us more..</label>
                </div>
                

     <div class="form-group"> 
        <div class="controls ">
         <textarea name="comment" rows="10" cols="40" class="textarea form-control"></textarea>
          </div>
           </div>

                <div class="form-group clearfix">
                    <div class="btn-group">
                        <button class="btn btn-success" name="submit_survey" type="submit" value="submit" id="survey-submit">Submit</button>
                    </div>
                </div>
            </form>




                @elseif(isset($iscommented) && $iscommented)
                <div class="title m-b-md">
                    Thanks for your feedback.
                </div>


                @else
                <div class="title m-b-md">
                    You gave us a rating of <span class="rating">{{ $rating }}</span>
                </div>
                <div class="title m-b-md">
                    Thanks for your feedback.
                </div>
                @endif
                
            </div>
        </div>
    </body>
</html>
