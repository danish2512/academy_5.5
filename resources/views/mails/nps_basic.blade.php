    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html>
        <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <title>Acadgild</title>
        <style type="text/css">
   
    </style>
        </head>
        <body>
    <div style="margin:0;padding:0;width:100%" width="100%">
            <table border="0" cellpadding="0" cellspacing="0" style="line-height:24px;margin:0;padding:0;width:100%" width="100%">
        <tbody><tr>
            <td align="center">
                <table border="0" cellpadding="0" cellspacing="0" style="margin:0 auto;width:640px" width="640">
                    <tbody><tr>
                        <td align="center" style="background-color:#fff;border-radius:3px" bgcolor="#ffffff">
                            


    <table border="0" cellpadding="0" cellspacing="0"style="margin:0;min-width:640px;width:100%" width="100%">
        <tbody><tr>
            <td align="center" valign="middle" style="height:40px;padding:19px 0" height="40">
                
                
                    <h3 style="font-family:&quot;Helvetica Neue&quot;,Helvetica,Arial,sans-serif;font-size:32px;font-weight:bold;line-height:40px;margin:0;padding:0">Acadgild </h3>
                
            </td>
        </tr>
        <tr>
            <td align="center" style="background-color:#fafafa;border-left:1px solid #ccc;border-right:1px solid #ccc;border-top:1px solid #ccc;padding:9px 0" bgcolor="#fafafa">
                
                <table align="center" border="0" cellpadding="0" cellspacing="0" style="width:602px" width="602">
                    <tbody><tr>
                        <td align="left" style="color:#111;font-family:&quot;Helvetica Neue&quot;,Helvetica,Arial,sans-serif;font-size:12px;font-weight:normal;line-height:14px;text-align:left">
                            <p></p>
    <p>Please take one minute to leave us your candid feedback so we can continue to improve. Our team will read each and every response. Don't hold back, we want to know what you really think. Thank you!</p>
                        </td>
                    </tr>
                </tbody></table>
                
            </td>
        </tr>
        <tr>
            <td align="center" style="background-color:#fafafa;border-left:1px solid #ccc;border-right:1px solid #ccc;padding:9px 0" bgcolor="#fafafa">
                <table align="center" border="0" cellpadding="0" cellspacing="0" style="width:602px" width="602">
                    <tbody><tr>
                        <td align="center" style="color:#111;font-family:Helvetica,Arial,sans-serif;font-size:18px;font-weight:bold;line-height:24px">
                            
                                How likely are you to recommend <br><span>Acadgild</span>  to a friend or colleague?
                            
                        </td>
                    </tr>
                </tbody></table>
            </td>
        </tr>
        <tr>
            <td align="center" style="background-color:#fafafa;border-left:1px solid #ccc;border-right:1px solid #ccc;display:none;float:left;line-height:0;overflow:hidden" bgcolor="#fafafa">Not at all likely</td>
        </tr>
        <tr>
            <td style="background-color:#fafafa;border-left:1px solid #ccc;border-right:1px solid #ccc;min-width:640px;padding:10px 0 0 0" bgcolor="#fafafa">
            <table align="center" style="border-spacing:0;min-width:640px;width:640px" width="640">
                <tbody><tr>
                    <td style="width:19px"></td>
                    <td>
                        
                            <table align="left" border="0" cellpadding="0" cellspacing="0" style="height:49px;width:49px" height="49" width="49">
                                <tbody><tr>
                                    <td align="center" valign="center" width="47" style="background-color:#fff;border:1px solid #ccc;border-bottom:2px solid #ccc;border-radius:5px;display:block;height:45px;width:47px" bgcolor="#ffffff" height="45">
                                        
                                        
                                        
                                        
                                        <a href='{{ $reviewURL. "/0" }}' style="color:#8d949c;display:block;font-family:&quot;Helvetica Neue&quot;,Helvetica,Arial,sans-serif;font-size:16px;font-weight:normal;line-height:45px;text-decoration:none" target="_blank">0</a>
                                        
                                    </td>
                                </tr>
                            </tbody></table>
                            
                                <table align="left" border="0" cellpadding="0" cellspacing="0" style="height:44px;width:5px" height="44" width="5">
                                    <tbody><tr>
                                        <td style="font-size:0;height:44px;line-height:0;width:4px" height="44" width="4">&nbsp;</td>
                                    </tr>
                                </tbody></table>
                            
                        
                            <table align="left" border="0" cellpadding="0" cellspacing="0"  style="height:49px;width:49px" height="49" width="49">
                                <tbody><tr>
                                    <td align="center" valign="center" width="47" style="background-color:#fff;border:1px solid #ccc;border-bottom:2px solid #ccc;border-radius:5px;display:block;height:45px;width:47px" bgcolor="#ffffff" height="45">
                                      
                                        
                                        <a href='{{ $reviewURL. "/1" }}' style="color:#8d949c;display:block;font-family:&quot;Helvetica Neue&quot;,Helvetica,Arial,sans-serif;font-size:16px;font-weight:normal;line-height:45px;text-decoration:none" target="_blank">1</a>
                                        
                                    </td>
                                </tr>
                            </tbody></table>
                            
                                <table align="left" border="0" cellpadding="0" cellspacing="0" style="height:44px;width:5px" height="44" width="5">
                                    <tbody><tr>
                                        <td style="font-size:0;height:44px;line-height:0;width:4px" height="44" width="4">&nbsp;</td>
                                    </tr>
                                </tbody></table>
                            
                        
                            <table align="left" border="0" cellpadding="0" cellspacing="0" style="height:49px;width:49px" height="49" width="49">
                                <tbody><tr>
                                    <td align="center" valign="center" width="47" style="background-color:#fff;border:1px solid #ccc;border-bottom:2px solid #ccc;border-radius:5px;display:block;height:45px;width:47px" bgcolor="#ffffff" height="45">
                                      
                                        <a href='{{ $reviewURL. "/2" }}' style="color:#8d949c;display:block;font-family:&quot;Helvetica Neue&quot;,Helvetica,Arial,sans-serif;font-size:16px;font-weight:normal;line-height:45px;text-decoration:none" target="_blank">2</a>
                                        
                                    </td>
                                </tr>
                            </tbody></table>
                            
                                <table align="left" border="0" cellpadding="0" cellspacing="0"  style="height:44px;width:5px" height="44" width="5">
                                    <tbody><tr>
                                        <td style="font-size:0;height:44px;line-height:0;width:4px" height="44" width="4">&nbsp;</td>
                                    </tr>
                                </tbody></table>
                            
                        
                            <table align="left" border="0" cellpadding="0" cellspacing="0" style="height:49px;width:49px" height="49" width="49">
                                <tbody><tr>
                                    <td align="center" valign="center" width="47" style="background-color:#fff;border:1px solid #ccc;border-bottom:2px solid #ccc;border-radius:5px;display:block;height:45px;width:47px" bgcolor="#ffffff" height="45">
                                        
                                        
                                        
                                        
                                        <a href='{{ $reviewURL. "/3" }}' style="color:#8d949c;display:block;font-family:&quot;Helvetica Neue&quot;,Helvetica,Arial,sans-serif;font-size:16px;font-weight:normal;line-height:45px;text-decoration:none" target="_blank">3</a>
                                        
                                    </td>
                                </tr>
                            </tbody></table>
                            
                                <table align="left" border="0" cellpadding="0" cellspacing="0"  style="height:44px;width:5px" height="44" width="5">
                                    <tbody><tr>
                                        <td style="font-size:0;height:44px;line-height:0;width:4px" height="44" width="4">&nbsp;</td>
                                    </tr>
                                </tbody></table>
                            
                        
                            <table align="left" border="0" cellpadding="0" cellspacing="0"  style="height:49px;width:49px" height="49" width="49">
                                <tbody><tr>
                                    <td align="center" valign="center" width="47" style="background-color:#fff;border:1px solid #ccc;border-bottom:2px solid #ccc;border-radius:5px;display:block;height:45px;width:47px" bgcolor="#ffffff" height="45">
                                        
                                        
                                        
                                        
                                        <a href='{{ $reviewURL. "/4" }}' style="color:#8d949c;display:block;font-family:&quot;Helvetica Neue&quot;,Helvetica,Arial,sans-serif;font-size:16px;font-weight:normal;line-height:45px;text-decoration:none" target="_blank">4</a>
                                        
                                    </td>
                                </tr>
                            </tbody></table>
                            
                                <table align="left" border="0" cellpadding="0" cellspacing="0"  style="height:44px;width:5px" height="44" width="5">
                                    <tbody><tr>
                                        <td style="font-size:0;height:44px;line-height:0;width:4px" height="44" width="4">&nbsp;</td>
                                    </tr>
                                </tbody></table>
                            
                        
                            <table align="left" border="0" cellpadding="0" cellspacing="0"  style="height:49px;width:49px" height="49" width="49">
                                <tbody><tr>
                                    <td align="center" valign="center" width="47" style="background-color:#fff;border:1px solid #ccc;border-bottom:2px solid #ccc;border-radius:5px;display:block;height:45px;width:47px" bgcolor="#ffffff" height="45">
                                        
                                        
                                        
                                        
                                        <a href='{{ $reviewURL. "/5" }}' style="color:#8d949c;display:block;font-family:&quot;Helvetica Neue&quot;,Helvetica,Arial,sans-serif;font-size:16px;font-weight:normal;line-height:45px;text-decoration:none" target="_blank">5</a>
                                        
                                    </td>
                                </tr>
                            </tbody></table>
                            
                                <table align="left" border="0" cellpadding="0" cellspacing="0"  style="height:44px;width:5px" height="44" width="5">
                                    <tbody><tr>
                                        <td style="font-size:0;height:44px;line-height:0;width:4px" height="44" width="4">&nbsp;</td>
                                    </tr>
                                </tbody></table>
                            
                        
                            <table align="left" border="0" cellpadding="0" cellspacing="0" style="height:49px;width:49px" height="49" width="49">
                                <tbody><tr>
                                    <td align="center" valign="center" width="47" style="background-color:#fff;border:1px solid #ccc;border-bottom:2px solid #ccc;border-radius:5px;display:block;height:45px;width:47px" bgcolor="#ffffff" height="45">
                                        
                                        
                                        
                                        
                                        <a href='{{ $reviewURL. "/6" }}' style="color:#8d949c;display:block;font-family:&quot;Helvetica Neue&quot;,Helvetica,Arial,sans-serif;font-size:16px;font-weight:normal;line-height:45px;text-decoration:none" target="_blank">6</a>
                                        
                                    </td>
                                </tr>
                            </tbody></table>
                            
                                <table align="left" border="0" cellpadding="0" cellspacing="0"  style="height:44px;width:5px" height="44" width="5">
                                    <tbody><tr>
                                        <td style="font-size:0;height:44px;line-height:0;width:4px" height="44" width="4">&nbsp;</td>
                                    </tr>
                                </tbody></table>
                            
                        
                            <table align="left" border="0" cellpadding="0" cellspacing="0"  style="height:49px;width:49px" height="49" width="49">
                                <tbody><tr>
                                    <td align="center" valign="center" width="47" style="background-color:#fff;border:1px solid #ccc;border-bottom:2px solid #ccc;border-radius:5px;display:block;height:45px;width:47px" bgcolor="#ffffff" height="45">
                                        
                                        
                                        
                                        
                                        <a href='{{ $reviewURL. "/7" }}'  style="color:#8d949c;display:block;font-family:&quot;Helvetica Neue&quot;,Helvetica,Arial,sans-serif;font-size:16px;font-weight:normal;line-height:45px;text-decoration:none" target="_blank">7</a>
                                        
                                    </td>
                                </tr>
                            </tbody></table>
                            
                                <table align="left" border="0" cellpadding="0" cellspacing="0"  style="height:44px;width:5px" height="44" width="5">
                                    <tbody><tr>
                                        <td style="font-size:0;height:44px;line-height:0;width:4px" height="44" width="4">&nbsp;</td>
                                    </tr>
                                </tbody></table>
                            
                        
                            <table align="left" border="0" cellpadding="0" cellspacing="0"  style="height:49px;width:49px" height="49" width="49">
                                <tbody><tr>
                                    <td align="center" valign="center" width="47" style="background-color:#fff;border:1px solid #ccc;border-bottom:2px solid #ccc;border-radius:5px;display:block;height:45px;width:47px" bgcolor="#ffffff" height="45">
                                        
                                        
                                        
                                        
                                        <a href='{{ $reviewURL. "/8" }}' style="color:#8d949c;display:block;font-family:&quot;Helvetica Neue&quot;,Helvetica,Arial,sans-serif;font-size:16px;font-weight:normal;line-height:45px;text-decoration:none" target="_blank">8</a>
                                        
                                    </td>
                                </tr>
                            </tbody></table>
                            
                                <table align="left" border="0" cellpadding="0" cellspacing="0"  style="height:44px;width:5px" height="44" width="5">
                                    <tbody><tr>
                                        <td style="font-size:0;height:44px;line-height:0;width:4px" height="44" width="4">&nbsp;</td>
                                    </tr>
                                </tbody></table>
                            
                        
                            <table align="left" border="0" cellpadding="0" cellspacing="0"  style="height:49px;width:49px" height="49" width="49">
                                <tbody><tr>
                                    <td align="center" valign="center" width="47" style="background-color:#fff;border:1px solid #ccc;border-bottom:2px solid #ccc;border-radius:5px;display:block;height:45px;width:47px" bgcolor="#ffffff" height="45">
                                        
                                        
                                        
                                        
                                        <a href='{{ $reviewURL. "/9" }}' style="color:#8d949c;display:block;font-family:&quot;Helvetica Neue&quot;,Helvetica,Arial,sans-serif;font-size:16px;font-weight:normal;line-height:45px;text-decoration:none" target="_blank">9</a>
                                        
                                    </td>
                                </tr>
                            </tbody></table>
                            
                                <table align="left" border="0" cellpadding="0" cellspacing="0"  style="height:44px;width:5px" height="44" width="5">
                                    <tbody><tr>
                                        <td style="font-size:0;height:44px;line-height:0;width:4px" height="44" width="4">&nbsp;</td>
                                    </tr>
                                </tbody></table>
                            
                        
                            <table align="left" border="0" cellpadding="0" cellspacing="0"  style="height:49px;width:49px" height="49" width="49">
                                <tbody><tr>
                                    <td align="center" valign="center" width="47" style="background-color:#fff;border:1px solid #ccc;border-bottom:2px solid #ccc;border-radius:5px;display:block;height:45px;width:47px" bgcolor="#ffffff" height="45">
                                        
                                        
                                        
                                        
                                        <a href='{{ $reviewURL. "/10" }}' style="color:#8d949c;display:block;font-family:&quot;Helvetica Neue&quot;,Helvetica,Arial,sans-serif;font-size:16px;font-weight:normal;line-height:45px;text-decoration:none" target="_blank" >10</a>
                                        
                                    </td>
                                </tr>
                            </tbody></table>
                            
                        
                    </td>
                    <td style="width:19px"></td>
                </tr>
            </tbody></table>
            </td>
        </tr>
        <tr>
            <td align="center" style="background-color:#fafafa;border-left:1px solid #ccc;border-right:1px solid #ccc;display:none;float:left;line-height:0;overflow:hidden" bgcolor="#fafafa">Extremely likely</td>
        </tr>
        <tr>
            <td  style="background-color:#fafafa;border-bottom:1px solid #ccc;border-left:1px solid #ccc;border-right:1px solid #ccc;padding:18px 0;width:100%" bgcolor="#fafafa" width="100%">
                <table border="0" cellpadding="0" cellspacing="0" align="center" style="min-width:640px;width:640px" width="640">
                    <tbody><tr>
                        <td style="color:#8d949c;font-family:&quot;Helvetica Neue&quot;,Helvetica,Arial,sans-serif;font-size:18px;min-width:19px"></td>
                        <td align="left" style="color:#333;font-family:&quot;Helvetica Neue&quot;,Helvetica,Arial,sans-serif;font-size:12px;width:50%" width="50%">Not at all likely</td>
                        <td align="right" style="color:#333;font-family:&quot;Helvetica Neue&quot;,Helvetica,Arial,sans-serif;font-size:12px;width:50%" width="50%">Extremely likely</td>
                        <td  style="color:#8d949c;font-family:&quot;Helvetica Neue&quot;,Helvetica,Arial,sans-serif;font-size:18px;min-width:19px"></td>
                    </tr>
                </tbody></table>
            </td>
        </tr>
    </tbody></table>


                        </td>
                    </tr>
                </tbody></table>

            </td>
        </tr>
        <tr>
            <td  style="height:46px" height="46">
                <table border="0" cellpadding="0" cellspacing="0" align="center" style="margin:0 auto;width:640px" width="640">
                    <tbody><tr>
                        

    <td align="left" valign="middle" style="color:#8d949c;font-family:&quot;Helvetica Neue&quot;,Helvetica,Arial,sans-serif;font-size:12px;height:50px;width:40%" height="50" width="40%">
        <table border="0" cellpadding="0" cellspacing="0">
            <tbody><tr>
                <td align="left">
                    <a href='{{ $unsubURL }}' style="color:#8d949c;text-decoration:none" target="_blank" >Unsubscribe</a>
                </td>
            </tr>
        </tbody></table>
    </td>

    <td align="right" valign="middle" style="color:#8d949c;font-family:&quot;Helvetica Neue&quot;,Helvetica,Arial,sans-serif;font-size:12px;height:50px;width:60%" height="50" width="60%">
        <table border="0" cellpadding="0" cellspacing="0">
            <tbody><tr>
              
            </tr>
        </tbody></table>
    </td>




                    </tr>
                </tbody></table>
            </td>
        </tr>
    </tbody></table>


   


    <div style="display:none;font:15px courier;line-height:0;white-space:nowrap">
    &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
    &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
    &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
    </div>
    </div>
    </body>
    </html>