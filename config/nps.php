<?php

return array(
	'default_nps_count' => 2,
	'scheduler_interval' => [
		2 => [.25, .75],
		3 => [.3, .66, 1],
		4 => [.25, .5, .75, 1],
		5 => [.2, .4, .6, .8, 1]
	],
	'passive_rating' => 7,
	'minimum_rating' => 0,
	'maximum_rating' => 10

);
