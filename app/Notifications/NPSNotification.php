<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class NPSNotification extends Notification implements ShouldQueue
{
    use Queueable;
    protected $npsId;
    protected $batchId;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($npsId, $batchId)
    {
        $this->npsId = $npsId;
        $this->batchId = $batchId;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database', 'mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        
        /*return (new MailMessage)
                    ->line('The introduction to the notification.')
                    ->action('Notification Action', url('/'))
                    ->line('Thank you for using our application!');*/
        return (new MailMessage)
                    ->view('mails.nps_basic', 
                        [
                            'reviewURL' => url('nps/basic/' . $this->id),
                            'unsubURL'  => url('mail/unsubscribe/' . $this->id)
                        ])
                    ->subject('Acadgild User Review');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'nps_id' => $this->npsId,
            'batch_id' => $this->batchId
        ];
    }
}
