<?php

namespace App;

use App\traits\user\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Models\Batch;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function batches()
    {
        return $this->belongsToMany(Batch::class, 'student_batch_assigned', 'student_id', 'batch_id');
    }
}
