<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Batch;

class NPS extends Model
{
    protected $table = 'nps';

	protected $primaryKey = 'nps_id';

	public function batch() {
		return $this->belongsTo( Batch::class, 'batch_id' );
	}
}
