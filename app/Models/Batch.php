<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\User;
use App\Models\NPS;

class Batch extends Model
{

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'batch';
	
	public function user() {
		return $this->belongsTo ( User::class, 'mentor_id' );
	}
	
	public function students(){
	    return $this->belongsToMany(User::class,'student_batch_assigned','batch_id','student_id');
    }

    public function nps() {
    	return $this->hasMany(NPS::class, 'batch_id');
    }

		
}
