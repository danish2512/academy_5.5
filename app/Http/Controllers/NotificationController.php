<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\DatabaseNotification;

class NotificationController extends Controller
{

	public function NPSReview($id, $rating){
		$notification = DatabaseNotification::findOrFail($id);
		if(isset($notification->data['rating'])){
			if($notification->data['rating'] >=Config::get('nps.minimum_rating') && $notification->data['rating'] < Config::get('nps.passive_rating')  && !isset($notification->data['comment'])){
				return view('feedbacks.nps_basic_user_review', ['showComment' => true,'notification' => $notification]);
			} else{
				return view('feedbacks.nps_basic_user_review', ['isreviewed' => true]);
			}
		} 
		else{
			$rate = (int)$rating;
			if($rate >=Config::get('nps.minimum_rating') && $rate <= Config::get('nps.maximum_rating')){
				$feedbackData = [];
				$feedbackData['rating'] = $rate;
				$notification->data = array_merge($feedbackData, $notification->data);
				$notification->save();
				if($rate < Config::get('nps.passive_rating')){
					return view('feedbacks.nps_basic_user_review', ['showComment' => true,
						'notification' => $notification
					]);
				}
				return view('feedbacks.nps_basic_user_review', ['rating' => $rate]);
			} else{
				abort('500');
			}
		}
	}

	public function addComment(Request $request){
		$notification = DatabaseNotification::findOrFail($request->input('notification_id'));
		$feedbackData = [];
		$feedbackData['comment'] = $request->input('comment') ? $request->input('comment') : '';
		$notification->data = array_merge($feedbackData, $notification->data);
		$notification->save();
		return view('feedbacks.nps_basic_user_review', ['iscommented' => true]);
	}

	public function unsubscribeUserMail($id){

	}
}
