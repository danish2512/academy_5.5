<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Notifications\NPSNotification;
use App\User;
use App\Models\NPS;
use Log;

class NPSCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cron:nps-basic';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send mail to user for calculating net promoter score NPS via user mail rating response';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */

    public function handle()
    {
        $today  = \Carbon\Carbon::today();
        $npsCollection = NPS::with(['batch.students' => function($q){$q->select('users.id', 'users.email');}])->where('status', false)->whereDate('scheduled_at', '<=', $today)->get();
        foreach ($npsCollection as $nps) {
            foreach($nps->batch->students as $user){
                $user = User::first();/*remove this line in production*/
                $user->notify(new NPSNotification($nps->nps_id, $nps->batch_id));
            }
            $nps->status = true;
            $nps->save();
        }
    }
}
